library(caret)

str(Auto)
attach(Auto)
mpg
horsepower2 <- (horsepower**2)
plot(mpg~(horsepower2))
par(mfrow=c(2,2))
plot(mpg~(horsepower))
plot(mpg~(horsepower2))
plot(horsepower~mpg)
plot(horsepower2~mpg)
dim(Auto)
str(Auto)
head(Auto)
set.seed(123)
smp_size <- floor(.623*nrow(Auto))
train_ind <- sample(seq_len(nrow(Auto)),size=smp_size)
train <- Auto[train_ind,]
test <- Auto[-train_ind,]
train
test
str(train)
str(test)
lm.fit <- lm(mpg~.-name,data=train)
summary(lm.fit)
lm.fit2 <- lm(mpg~displacement+weight+acceleration+year+year+origin)
summary(lm.fit2)
lm.fit3 <- lm(mpg~weight+year+year+origin)
summary(lm.fit3)
